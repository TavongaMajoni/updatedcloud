//updated at 02:39

(function(){
    //Initialize Firebase
    const config = {
        apiKey: "AIzaSyCLIw6MBDDFEJUxJ4mRxNq-ucs2jKUDrDs",
        authDomain: "auction-354f7.firebaseapp.com",
        databaseURL: "https://auction-354f7.firebaseio.com",
        projectId: "auction-354f7",
        storageBucket: "auction-354f7.appspot.com",
        messagingSenderId: "455792279663",
        appId: "1:455792279663:web:a519038d7672a3f4a88d9a"
    };
    firebase.initializeApp(config);
    

    //Get form details
    const txtEmail = document.getElementById('exampleInputEmail1');
    const txtPassword = document.getElementById('exampleInputPassword1');
    const btnLogin = document.getElementById('btnLogin');
    const btnSignUp = document.getElementById('btnSignUp');
    const btnLogout = document.getElementById('btnLogout');


    //Get Auction form details
    const txtAuctAmount = document.getElementById('auct_amount');
    const txtAuctInterest = document.getElementById('auct_interest');
    const txtAuctDuration = document.getElementById('auct_duration');

    //event for login
    if(btnLogin){
    
        btnLogin.addEventListener('click', e=>{
            //get email and pass
            const email = txtEmail.value;
            const pass = txtPassword.value;
            const auth = firebase.auth();   

            //sign in
            const promise = auth.signInWithEmailAndPassword(email, pass);
            promise.catch(e=> console.log(e.message));
        });
    }

    //event for sign up
    if(btnSignUp){

    btnSignUp.addEventListener('click', e=>{
        //get email and pass
        const email = txtEmail.value;
        const pass = txtPassword.value;
        const auth = firebase.auth();

        //sign in
        const promise = auth.createUserWithEmailAndPassword(email, pass);
        promise.catch(e=> console.log(e.message));
	  
    } );}

    //event for logout
    if(btnLogout){

   btnLogout.addEventListener('click', e=>{
        //logout
        firebase.auth().signOut();
        
    } );}

    if(btnAuction){

    btnAuction.addEventListener('click',e=>{
        console.log("am here")
        const amount = txtAuctAmount.value;
        const rate = txtAuctInterest.value;
        const time = txtAuctDuration.value;

        // Add a new document in collection "auctions"
        // db.collection("auctions").add({
        //     amount: amount,
        //     rate: rate,
        //     time: time
        // })
        // .then(function() {
        //     console.log("Document successfully written!");
        // })
        // .catch(function(error) {
        //     console.error("Error writing document: ", error);
        // });

    });}

}());

//live update user bids
//create bids page
//dynamically update bids page
//design classes for database
//auS
